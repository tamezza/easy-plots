/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

// Always protect against multiple includes!
#ifndef HHBBYYANALYSIS_FINALVARSYYBBALG
#define HHBBYYANALYSIS_FINALVARSYYBBALG

#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysWriteDecorHandle.h>
#include <SystematicsHandles/SysReadDecorHandle.h>

#include <AthContainers/ConstDataVector.h>

#include <AthenaBaseComps/AthHistogramAlgorithm.h>
#include <FourMomUtils/xAODP4Helpers.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODMissingET/MissingETContainer.h>

namespace HHBBYY
{

  /// \brief An algorithm for counting containers
  class BaselineVarsbbyyAlg final : public AthHistogramAlgorithm
  {
    /// \brief The standard constructor
  public:
    BaselineVarsbbyyAlg(const std::string &name, ISvcLocator *pSvcLocator);

    /// \brief Initialisation method, for setting up tools and other persistent
    /// configs
    StatusCode initialize() override;
    /// \brief Execute method, for actions to be taken in the event loop
    StatusCode execute() override;
    /// We use default finalize() -- this is for cleanup, and we don't do any

    float compute_Topness(const xAOD::JetContainer *jets);
    float* compute_EventShapes(std::unique_ptr<ConstDataVector<xAOD::JetContainer>> &bjets, const xAOD::PhotonContainer *photons);
    float compute_pTBalance(std::unique_ptr<ConstDataVector<xAOD::JetContainer>> &bjets, const xAOD::PhotonContainer *photons);

  private:
    // ToolHandle<whatever> handle {this, "pythonName", "defaultValue",
    // "someInfo"};

    /// \brief Setup syst-aware input container handles
    CP::SysListHandle m_systematicsList {this};

    CP::SysReadHandle<xAOD::JetContainer>
    m_jetHandle{ this, "jets", "",   "Jet container to read" };

    CP::SysReadDecorHandle<char> 
    m_isBtag {this, "bTagWPDecorName", "", "Name of input decorator for b-tagging"};

    CP::SysReadDecorHandle<int> 
    m_PCBT {this, "PCBTDecorName", "", "Name of pseudo-continuous b-tagging decorator"};

    CP::SysReadHandle<xAOD::PhotonContainer>
    m_photonHandle{ this, "photons", "",   "Photons container to read" };

    CP::SysReadHandle<xAOD::ElectronContainer>
    m_electronHandle{ this, "electrons", "",   "Electron container to read" };

    CP::SysReadHandle<xAOD::MuonContainer>
    m_muonHandle{ this, "muons", "",   "Muon container to read" };

    Gaudi::Property<std::string> m_photonWPName
      { this, "photonWP", "","Photon ID + Iso working point" };
    CP::SysReadDecorHandle<float> m_ph_SF{"", this};

    CP::SysReadHandle<xAOD::MissingETContainer>
    m_metHandle{ this, "met", "",   "MET container to read" };

    CP::SysReadHandle<xAOD::EventInfo>
    m_eventHandle{ this, "event", "EventInfo",   "EventInfo container to read" };

    Gaudi::Property<bool> m_isMC
      { this, "isMC", false, "Is this simulation?" };

    std::unordered_map<std::string, CP::SysWriteDecorHandle<float> > m_Fbranches;
    std::vector<std::string> m_Fvarnames{
      // Leading/Subleading photon kinematics
      "Photon1_pt", "Photon1_eta", "Photon1_phi", "Photon1_E",
      "Photon2_pt", "Photon2_eta", "Photon2_phi", "Photon2_E",

      "myy", "pTyy", "dRyy", "Etayy", "Phiyy", 

      // HbbCandidate jet kinematics
      "HbbCandidate_Jet1_pt", "HbbCandidate_Jet1_phi", "HbbCandidate_Jet1_eta", "HbbCandidate_Jet1_E",
      "HbbCandidate_Jet2_pt", "HbbCandidate_Jet2_phi", "HbbCandidate_Jet2_eta", "HbbCandidate_Jet2_E",

      "mbb", "pTbb", "dRbb", "Etabb", "Phibb", 

      // Inclusive jet kinematics
      "Jet1_pt", "Jet1_eta", "Jet1_phi", "Jet1_E",
      "Jet2_pt", "Jet2_eta", "Jet2_phi", "Jet2_E",
      "Jet3_pt", "Jet3_eta", "Jet3_phi", "Jet3_E",
      "Jet4_pt", "Jet4_eta", "Jet4_phi", "Jet4_E",

      // di-higgs variables
      "mbbyy", "pTbbyy", "dRbbyy", "Etabbyy", "Phibbyy", "mbbyy_star", "Photon1_ptOvermyy", "Photon2_ptOvermyy",

      // mva variables
        "HT", "topness", "sphericityT", "planarFlow", "pTBalance", "missEt", "metphi",

      // VBF jets kinematics
      "Jet_vbf_j1_pt", "Jet_vbf_j1_eta", "Jet_vbf_j1_phi", "Jet_vbf_j1_E",
      "Jet_vbf_j2_pt", "Jet_vbf_j2_eta", "Jet_vbf_j2_phi", "Jet_vbf_j2_E",
      "Jet_vbf_jj_m", "Jet_vbf_jj_deta",
      "Jet_vbf_j1_yybb_dR", "Jet_vbf_j2_yybb_dR", "Jet_vbf_j1_yybb_deta", "Jet_vbf_j2_yybb_deta",
      "Jet_vbf_jj_yybb_dR", "Jet_vbf_jj_yybb_deta", 
      "Jet_vbf_jj_yybb_pT", "Jet_vbf_jj_yybb_eta", "Jet_vbf_jj_yybb_phi", "Jet_vbf_jj_yybb_m"
    };

    std::vector<std::string> m_Fvarnames_MC{
      "Photon1_effSF", "Photon2_effSF"
    };

    std::unordered_map<std::string, CP::SysWriteDecorHandle<int> > m_Ibranches;
    std::vector<std::string> m_Ivarnames{
      "nPhotons", "nBJets", "nJets", "nCentralJets", "nLeptons",
      // leading and subleading btagged jets
      "HbbCandidate_Jet1_truthLabel", "HbbCandidate_Jet2_truthLabel",
      "HbbCandidate_Jet1_pcbt", "HbbCandidate_Jet2_pcbt",
      // leading and subleading jets
      "Jet1_truthLabel", "Jet2_truthLabel",
      "Jet3_truthLabel", "Jet4_truthLabel",
      "Jet1_PassWP", "Jet2_PassWP",
      "Jet3_PassWP", "Jet4_PassWP",
      "Jet1_pcbt", "Jet2_pcbt",
      "Jet3_pcbt", "Jet4_pcbt"
    };

  };
}
#endif
